# ATLAS PanDA Tools

## Prerequisite

This tool uses the API to collect information from https://bigpanda.cern.ch/ which provides some more flexibility in the querying of jobs.

You'll need a cookie for https://bigpanda.cern.ch ; it is recommended you run on `lxplus` or transfer the cookie locally.

To get the cookie, run (from the top level of the clone of the repository)

```
cern-get-sso-cookie -u https://bigpanda.cern.ch/ -o bigpanda.cookie.txt
```

## Usage

Setup:

```
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

lsetup panda
voms-proxy-init -voms atlas
```

Then you can run, for example:

```
./panda_job_monitor.py -q "date_from=2021-03-15&taskname=user.karolos.*_13TeV.*SSWW.sys.V5.1*"
```

```
./panda_job_monitor.py -q "date_from=2021-03-15&taskname=user.karolos.*_13TeV.*SSWW.V5.1*"
```

## Replicating output datasets on an RSE

To replicate the output datasets (only for 'done' jobs), use:

```
./panda_job_monitor.py -q "date_from=2021-03-15&taskname=user.karolos.*_13TeV.*SSWW.sys.V5.1*" --rse DESY-HH_LOCALGROUPDISK
```

Note that this will only create replication rules for the datasets from 'done' jobs.
