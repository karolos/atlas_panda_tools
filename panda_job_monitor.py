#!/usr/bin/env python

import argparse
import json
import requests

from rucio.client.ruleclient import RuleClient
import rucio.common.exception

# cern-get-sso-cookie -u https://bigpanda.cern.ch/ -o bigpanda.cookie.txt
# curl -b bigpanda.cookie.txt -H 'Accept: application/json' -H 'Content-Type: application/json' "https://bigpanda.cern.ch/tasks/?date_from=2021-03-15&taskname=user.karolos.mc16_13TeV.*SSWW.sys.V5.1*&json"

url = 'https://bigpanda.cern.ch/tasks/?date_from=2021-03-15&taskname=user.karolos.mc16_13TeV.*SSWW.sys.V5.1*&json'
url = 'https://bigpanda.cern.ch/tasks/?date_from=2021-03-15&taskname=user.karolos.mc16_13TeV.*SSWW.V5.1*&json'
url = 'https://bigpanda.cern.ch/tasks/?date_from=2021-03-15&taskname=user.karolos.*_13TeV.*SSWW.sys.V5.1*&json'


def query_bigpanda( query ):
  headers = {'content-type': 'application/json', 'Accept': 'application/json'}
  #cookies = {'sessionid': '8l2mebs2c9msc067f4m98dt33af5qpxr'}
  
  with open("bigpanda.cookie.txt") as iF:
    for l in iF.readlines():
      if l.find("sessionid") != -1:
        session_id = l.split(" ")[-1]
  cookies = { 'sessionid' : session_id }
 
  url = "https://bigpanda.cern.ch/tasks/?json&" + query 
  r = requests.get(url, headers=headers)
  jobs = r.json()
  return jobs


def get_out_ds(jobs, status = "done"):
  def get_scope(did):
    return ".".join(did.split(".")[:2])

  out_ds_list = [ ]
  for j in jobs:
    #print(j)
    if j['status'] != "done": continue
    out_ds = { ds['containername'].strip('/') for ds in j['datasets'] if ds['type'] == 'output' }
    out_ds_list.extend([{'name' : did, 'scope' :get_scope(did) } for did in out_ds])
    #print(j['taskname'], out_ds)
  return out_ds_list

def display_job_info(jobs):
  info = { }
 
  ## For any sample not matching the mc16X campaign format 

  print("| Sample |")
  print("| ------ |")
  
  for j in jobs:
    if j['status'] == "broken": continue
    if j['taskname'].find(".mc16") == -1:
      print "| [{}](https://bigpanda.cern.ch/task/{}) [{} %]|".format( str(j['taskname']), j['jeditaskid'], j['pctfinished'])
    grpName = j['taskname'].split(".")[3]
    campaign = j['taskname'].split(".")[4]
    if not grpName in info: info[grpName] = { }
    info[grpName][campaign] = { 'taskid' : j['jeditaskid'], 'pct' : j['pctfinished'] }
  
  print("| Group | mc16a | mc16d | mc16d |")
  print("| ----- | ----- | ----- | ----- |")
  for g in info:
    try:
      print( "| {} | {} | {} | {} |".format( g,
  "[{}](https://bigpanda.cern.ch/task/{})".format( str(info[g]["mc16a"]["taskid"]) + " (" + str(info[g]["mc16a"]["pct"]) + "%)", str(info[g]["mc16a"]["taskid"]) ), 
  "[{}](https://bigpanda.cern.ch/task/{})".format( str(info[g]["mc16d"]["taskid"]) + " (" + str(info[g]["mc16d"]["pct"]) + "%)", str(info[g]["mc16d"]["taskid"]) ), 
  "[{}](https://bigpanda.cern.ch/task/{})".format( str(info[g]["mc16e"]["taskid"]) + " (" + str(info[g]["mc16e"]["pct"]) + "%)", str(info[g]["mc16e"]["taskid"]) ) ) )
    except KeyError:
      pass


def copy_to_rse( ds_list, rse ):
  ## Creating rucio rules
  try:
    rucioclient = RuleClient()
  except rucio.common.exception.CannotAuthenticate:
    print("Couldn't uythenticate to rucio -- did you run voms-proxy-init -voms atlas ?")
    return

  for d in ds_list:
    try:
      print(d)
      rucioclient.add_replication_rule([d], 1, rse)
      print("Replication rule for {} @ {}".format(d['name'], rse))
    except rucio.common.exception.DuplicateRule:
      print("WARNING: Already replicated {} @ {}".format(d['name'], rse))
      pass


if __name__ == '__main__':

    parser = argparse.ArgumentParser('panda_job_monitor_and_replicator')
    parser.add_argument('-q', '--query', help='query for bigpanda.cern.ch', required=True)
    parser.add_argument('--rse', help='Rucio Storage Element where to copy datasets to', required=False)

    args = parser.parse_args()

    jobs = query_bigpanda( args.query )   
    display_job_info( jobs )
    ds_list = get_out_ds( jobs )
    if args.rse: copy_to_rse( ds_list, args.rse )


